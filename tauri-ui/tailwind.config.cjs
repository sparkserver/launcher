/** @type {import('tailwindcss').Config} */
module.exports = {
  content: [
    "./index.html",
    "./src/**/*.vue"
  ],
  theme: {
    extend: {
      fontFamily: {
        'sans': ['Roboto']
      },
      transitionProperty: {
        'width': ['width'],
        'clip-path': ['clip-path']
      }
    },
  },
  plugins: [],
}
