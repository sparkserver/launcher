// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at https://mozilla.org/MPL/2.0/.

#![cfg_attr(
    all(not(debug_assertions), target_os = "windows"),
    windows_subsystem = "windows"
)]

use tauri::Manager;
use tauri::async_runtime;
use std::time::Duration;
use serde::Serialize;
use std::sync::Mutex;

#[tauri::command]
fn auth_login(server_url: &str, username: &str, password: &str) {
    println!("Login request");
}

#[derive(Serialize)]
#[serde(rename_all = "camelCase")]
struct DownloadProgress {
    pub downloaded_bytes: usize,
    pub total_bytes: usize
}

struct DlWorkerState {
    pub ch: Mutex<Option<async_std::channel::Sender<()>>>
}

async fn downloader_worker(app: tauri::AppHandle, ch: async_std::channel::Receiver<()>) {
    println!("DL worker start");
    let mut dl_bytes = 0;
    loop {
        app.emit_all("downloader_progress", &DownloadProgress{
            downloaded_bytes: dl_bytes,
            total_bytes: 1078 * 1024 * 1024
        });
        let res = async_std::future::timeout(Duration::from_secs(1), ch.recv()).await;
        match res {
            Err(_) => {},//timed out
            Ok(_) => break
        }
        dl_bytes += 8 * 1024 * 1024;
    }
    println!("DL worker exit");
}

#[tauri::command]
fn downloader_start(app: tauri::AppHandle, state: tauri::State<DlWorkerState>, verify: bool) {
    println!("Start downloader, verify = {}", verify);
    let (send, recv) = async_std::channel::unbounded();
    *state.ch.lock().unwrap() = Some(send);
    async_runtime::spawn(async { downloader_worker(app, recv).await });
}

#[derive(Serialize)]
#[serde(rename_all = "camelCase")]
struct Settings {
    pub game_path: String
}

#[tauri::command]
fn settings_get_settings(app: tauri::AppHandle) -> Settings {
    let config_path = app.path_resolver().app_dir().unwrap();
    Settings{
        game_path: config_path.join("gamefiles").to_string_lossy().into_owned()
    }
}

fn main() {
    tauri::Builder::default()
        .manage(DlWorkerState{ch: Mutex::new(None)})
        .invoke_handler(tauri::generate_handler![auth_login, downloader_start, settings_get_settings])
        .run(tauri::generate_context!())
        .expect("error while running tauri application");
}
