/**
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/.
 */

import { invoke } from "@tauri-apps/api/tauri";
import { EventCallback, listen, UnlistenFn } from "@tauri-apps/api/event";

export type AuthLoginRequest = {
    serverUrl: string;
    email: string;
    password: string;
}

export type DownloaderStartRequest = {
    verify: boolean;
}

export type DownloaderProgressUpdateEvent = {
    downloadedBytes: number;
    totalBytes: number;
}

export type GetSettingsResponse = {
    gamePath: string;
}

interface NativeAPI {
    auth: {
        login: (args: AuthLoginRequest) => Promise<void>
    },
    downloader: {
        onProgressUpdate: (handler: EventCallback<DownloaderProgressUpdateEvent>) => Promise<UnlistenFn>
        start: (args: DownloaderStartRequest) => Promise<void>
    },
    settings: {
        getSettings: () => Promise<GetSettingsResponse>
    }
}

export default {
    auth: {
        login: invoke.bind(null, 'auth_login')
    },
    downloader: {
        onProgressUpdate: listen.bind(null, 'downloader_progress'),
        start: invoke.bind(null, 'downloader_start')
    },
    settings: {
        getSettings: invoke.bind(null, 'settings_get_settings')
    }
} as NativeAPI;